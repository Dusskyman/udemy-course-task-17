import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:udemy_task_17/providers/data_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<DataProvider>(
      create: (_) => DataProvider(),
      builder: (context, child) => MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var functions = Provider.of<DataProvider>(context, listen: false);
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            SizedBox(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.79,
              child: Consumer<DataProvider>(
                builder: (context, value, child) => ListView.builder(
                  itemCount: value.list.length,
                  itemBuilder: (context, index) => Center(
                    child: SizedBox(
                      child: Text(
                        '${value.list[index]}',
                        style: TextStyle(fontSize: 35),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              color: Color.fromRGBO(39, 39, 39, 1),
              width: double.infinity,
              height: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  InkWell(
                    onTap: () {
                      functions.generateList();
                    },
                    child: Icon(
                      Icons.add_rounded,
                      color: Color.fromRGBO(255, 255, 255, 0.8),
                      size: 50,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      functions.sortList();
                    },
                    child: Icon(
                      Icons.shuffle_rounded,
                      color: Color.fromRGBO(255, 255, 255, 0.8),
                      size: 50,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      functions.removeRandom();
                    },
                    child: Icon(
                      Icons.remove_rounded,
                      color: Color.fromRGBO(255, 255, 255, 0.8),
                      size: 50,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
