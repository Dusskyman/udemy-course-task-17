import 'dart:math';

import 'package:flutter/widgets.dart';

class DataProvider with ChangeNotifier {
  List<int> list = [];
  bool switchSort = false;

  void generateList() {
    list = List.generate(100, (index) => index);
    notifyListeners();
  }

  void sortList() {
    if (switchSort) {
      list.sort((a, b) => a.compareTo(b));
      switchSort = !switchSort;
      notifyListeners();
    } else {
      list.sort((b, a) => a.compareTo(b));
      switchSort = !switchSort;
      notifyListeners();
    }
  }

  void removeRandom() {
    if (list.length != 0) {
      list.remove(Random().nextInt(list.length));
      notifyListeners();
    }
  }
}
